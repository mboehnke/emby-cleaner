use std::fmt::Display;

use apply::Apply;
use gazebo::prelude::*;
use pretend::{
    http::{
        header::{self, InvalidHeaderValue},
        HeaderValue,
    },
    pretend, request,
    resolver::UrlResolver,
    HeaderMap, Json, Pretend, Url,
};
use pretend_reqwest::{reqwest::ClientBuilder, Client};
use thiserror::Error;

pub use schema::*;

mod schema;

#[derive(Error, Debug)]
#[non_exhaustive]
pub enum EmbyError {
    #[error("cannot parse URL: {url:?}")]
    URLParseError {
        source: pretend::resolver::ParseError,
        url: String,
    },
    #[error("cannot parse header value: {val:?}")]
    HeaderValueParseError {
        source: InvalidHeaderValue,
        val: String,
    },
    #[error("cannot construct HTTP client")]
    ClientBuildError {
        source: pretend_reqwest::reqwest::Error,
    },
    #[error("API request failed: {0}")]
    ApiRequestError(String),
}

trait IntoString {
    fn into_string(&self) -> String;
}

impl IntoString for pretend::Error {
    fn into_string(&self) -> String {
        match self {
            pretend::Error::Client(_) => "Failed to create client".to_string(),
            pretend::Error::Request(_) => "Invalid request".to_string(),
            pretend::Error::Response(_) => "Failed to execute request".to_string(),
            pretend::Error::Body(_) => "Failed to read response body".to_string(),
            pretend::Error::Status(s) => format!("HTTP {}", s),
        }
    }
}

pub struct EmbyClient {
    base_url: Url,
    authorization: HeaderValue,
    api: Pretend<Client, UrlResolver>,
}

pub struct AuthEmbyClient {
    user_id: String,
    api: Pretend<Client, UrlResolver>,
}

#[pretend]
trait EmbyApi {
    #[request(method = "POST", path = "Users/AuthenticateByName")]
    async fn login(&self, json: Login) -> pretend::Result<Json<AuthenticationResult>>;

    #[request(method = "GET", path = "Library/SelectableMediaFolders")]
    async fn sections(&self) -> pretend::Result<Json<Vec<MediaFolder>>>;

    #[request(
        method = "GET",
        path = "Users/{user_id}/Items?fields=UserDataLastPlayedDate"
    )]
    async fn watched_videos(
        &self,
        user_id: &str,
        query: &ItemQuery,
    ) -> pretend::Result<Json<QueryResultBaseItemDto>>;

    #[request(method = "DELETE", path = "Items/{video_id}")]
    async fn delete_video(&self, video_id: &str) -> pretend::Result<String>;
}

impl EmbyClient {
    pub async fn new(
        base_url: &str,
        client: &str,
        device: &str,
        device_id: &str,
        version: &str,
    ) -> Result<Self, EmbyError> {
        let base_url = base_url
            .parse::<Url>()
            .map_err(|source| EmbyError::URLParseError {
                source,
                url: base_url.to_string(),
            })?;

        let auth_str = format!(
            "Emby Client=\"{}\", Device=\"{}\", DeviceId=\"{}\", Version=\"{}\"",
            client, device, device_id, version
        );
        let authorization: HeaderValue =
            auth_str
                .parse()
                .map_err(|source| EmbyError::HeaderValueParseError {
                    source,
                    val: auth_str,
                })?;

        let mut headers = HeaderMap::new();
        headers.insert(header::AUTHORIZATION, authorization.clone());
        let client = ClientBuilder::new()
            .default_headers(headers)
            .build()
            .map_err(|source| EmbyError::ClientBuildError { source })?
            .apply(Client::new);

        let api = Pretend::for_client(client).with_url(base_url.clone());

        Ok(EmbyClient {
            base_url,
            authorization,
            api,
        })
    }

    pub async fn login(
        self,
        username: impl Display,
        password: impl Display,
    ) -> Result<AuthEmbyClient, EmbyError> {
        let data = Login {
            username: username.to_string(),
            password: password.to_string(),
        };

        let auth_result: AuthenticationResult = self
            .api
            .login(data)
            .await
            .map_err(|source| EmbyError::ApiRequestError(source.into_string()))?
            .value();

        let token = auth_result.access_token.parse().map_err(|source| {
            EmbyError::HeaderValueParseError {
                source,
                val: "****access token redacted****".to_string(),
            }
        })?;
        let mut headers = HeaderMap::new();
        headers.insert(header::AUTHORIZATION, self.authorization);
        headers.insert("X-Emby-Token", token);
        let api = ClientBuilder::new()
            .default_headers(headers)
            .build()
            .map_err(|source| EmbyError::ClientBuildError { source })?
            .apply(Client::new)
            .apply(Pretend::for_client)
            .with_url(self.base_url);
        let user_id = auth_result.user.id;

        Ok(AuthEmbyClient { user_id, api })
    }
}

impl AuthEmbyClient {
    pub async fn sections(&self) -> Result<Vec<MediaFolder>, EmbyError> {
        self.api
            .sections()
            .await
            .map_err(|source| EmbyError::ApiRequestError(source.into_string()))
            .map(Json::value)
    }

    pub async fn watched_videos(&self, section_id: &str) -> Result<Vec<MediaItem>, EmbyError> {
        let query = ItemQuery {
            parent_id: section_id.to_string(),
            is_played: true,
            recursive: true,
        };
        self.api
            .watched_videos(&self.user_id, &query)
            .await
            .map_err(|source| EmbyError::ApiRequestError(source.into_string()))?
            .value()
            .items
            .into_map(MediaItem::from)
            .apply(Ok)
    }

    pub async fn delete_video(&self, video_id: &str) -> Result<(), EmbyError> {
        self.api
            .delete_video(video_id)
            .await
            .map_err(|source| EmbyError::ApiRequestError(source.into_string()))
            .map(|_| ())
    }
}
