use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct Login {
    pub username: String,
    pub password: String,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct ItemQuery {
    pub parent_id: String,
    pub is_played: bool,
    pub recursive: bool,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct UserDTO {
    pub id: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct AuthenticationResult {
    pub user: UserDTO,
    pub access_token: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct BaseItemDto {
    pub id: String,
    pub user_data: UserItemDataDto,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct QueryResultBaseItemDto {
    pub items: Vec<BaseItemDto>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct UserItemDataDto {
    pub last_played_date: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct MediaFolder {
    pub name: String,
    pub id: String,
}

#[derive(Debug)]
pub struct MediaItem {
    pub id: String,
    pub last_played_date: String,
}

impl From<BaseItemDto> for MediaItem {
    fn from(item: BaseItemDto) -> Self {
        MediaItem {
            id: item.id,
            last_played_date: item.user_data.last_played_date,
        }
    }
}
