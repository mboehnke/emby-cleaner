FROM rust as builder
COPY ./ ./
RUN cargo build --release


FROM gcr.io/distroless/cc-debian12
COPY --from=builder ./target/release/emby-cleaner /usr/bin/
CMD ["/usr/bin/emby-cleaner"]
