use anyhow::{Context, Result};
use serde::{Deserialize, Serialize};
use std::path::Path;

#[derive(Serialize, Deserialize, Debug)]
pub struct Settings {
    pub server_ip: String,
    pub server_port: u16,

    pub username: String,
    pub password: String,

    pub sections: Vec<String>,

    pub minutes_before_deletion: i64,
    pub minutes_between_scans: u64,

    pub log_level: String,
}

impl Settings {
    pub fn from_file<T: AsRef<Path>>(file: T) -> Result<Settings> {
        config::Config::new()
            .set_default("server_ip", "127.0.0.1")?
            .set_default("server_port", 8096)?
            .set_default("password", "")?
            .set_default("minutes_before_deletion", 300)?
            .set_default("minutes_between_scans", 15)?
            .set_default("log_level", "error")?
            .merge(config::File::from(file.as_ref()))
            .with_context(|| format!("failed to load settings from {:?}", file.as_ref()))?
            .to_owned()
            .try_into()
            .with_context(|| format!("failed to read valid settings from {:?}", file.as_ref()))
    }
}
