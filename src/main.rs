mod settings;

use anyhow::Context;
use apply::Apply;
use chrono::{DateTime, Duration, Utc};
use emby::{AuthEmbyClient, EmbyClient, MediaFolder, MediaItem};
use log::{error, info};
use mac_address::MacAddress;
use settings::Settings;
use uuid::Uuid;

#[cfg(debug_assertions)]
const SETTINGS_FILE: &str = "settings/settings.toml";
#[cfg(not(debug_assertions))]
const SETTINGS_FILE: &str = "/etc/emby-cleaner/settings.toml";

const DEVICE: &str = "PC";
const CLIENT: &str = env!("CARGO_PKG_NAME");
const VERSION: &str = env!("CARGO_PKG_VERSION");

async fn delete_watched_videos(settings: &Settings) -> Result<(), anyhow::Error> {
    let client = build_client(settings).await?;

    info!("fetching section ids for {:?}", settings.sections);
    let section_ids = client
        .sections()
        .await
        .context("could not get sections from emby server")?
        .into_iter()
        .filter(|section| settings.sections.contains(&section.name));

    let time_before_deletion = Duration::minutes(settings.minutes_before_deletion);
    info!(
        "time before deletion: {} minutes",
        time_before_deletion.num_minutes()
    );

    let now = Utc::now();

    for MediaFolder { id, name } in section_ids {
        info!("fetching videos for section: {:?}", name);
        let videos = client
            .watched_videos(&id)
            .await
            .with_context(|| format!("could not get videos for section: {:?}", name))?;

        for MediaItem {
            id,
            last_played_date,
        } in videos
        {
            // println!("{}: {:?}", id, last_played_date);
            let date: DateTime<Utc> = DateTime::parse_from_rfc3339(&last_played_date)
                .with_context(|| format!("could not parse date: {:?}", last_played_date))?
                .into();
            info!(
                "video {} has been played {} minutes ago",
                &id,
                (now - date).num_minutes(),
            );

            if date + time_before_deletion <= now {
                info!("deleting video {}", &id);
                client
                    .delete_video(&id)
                    .await
                    .context("could not delete video")?;
            }
        }
    }

    Ok(())
}

async fn build_client(settings: &Settings) -> anyhow::Result<AuthEmbyClient> {
    let base_url = format!(
        "http://{}:{}/emby",
        &settings.server_ip, &settings.server_port
    );
    info!("emby server api at: {}", &base_url);

    let device_id = Uuid::new_v5(
        &Uuid::NAMESPACE_URL,
        &mac_address::get_mac_address()
            .unwrap_or_default()
            .map(MacAddress::bytes)
            .unwrap_or_default(),
    )
    .simple()
    .to_string();

    info!("constructing emby client");
    let client = EmbyClient::new(&base_url, CLIENT, DEVICE, &device_id, VERSION)
        .await
        .context("could not construct emby client")?
        .login(&settings.username, &settings.password)
        .await?;
    Ok(client)
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // load config
    let mut settings = Settings::from_file(SETTINGS_FILE)?;
    // initialize logger
    env_logger::Builder::new()
        .parse_filters(&settings.log_level)
        .init();

    loop {
        // reload config in case it changed
        match Settings::from_file(SETTINGS_FILE) {
            Ok(s) => settings = s,
            Err(e) => error!(
                "could not reload config, continuing with previously loaded data: {:?}",
                e
            ),
        }

        if let Err(error) = delete_watched_videos(&settings).await {
            let error = error
                .chain()
                .map(|e| e.to_string())
                .collect::<Vec<_>>()
                .join("\n");
            error!("{}", error)
        }

        info!("sleeping for {} minutes", settings.minutes_between_scans);
        std::time::Duration::from_secs(settings.minutes_between_scans * 60)
            .apply(tokio::time::sleep)
            .await;
    }
}
